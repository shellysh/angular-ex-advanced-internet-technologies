import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersComponent } from './users/users.component';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { MessagesFormsComponent } from './messages/messages-forms/messages-forms.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { MessageFormComponent } from './messages/message-form/message-form.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    MessagesFormsComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    MessageFormComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
            {pathMatch: 'full',path: '', component: MessagesComponent},
            {pathMatch: 'full',path: 'users', component: UsersComponent},
            {pathMatch: 'full',path: 'message/:id', component: MessageComponent},
            {pathMatch: 'full',path: 'message-form/:id', component: MessageFormComponent},
            {pathMatch: 'full',path: '**', component: NotFoundComponent}
    ])
  ],
  providers: [
    MessagesService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
