import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { MessagesService } from './../messages.service';
import {FormGroup , FormControl} from '@angular/forms';

@Component({
  selector: 'messagesForms',
  templateUrl: './messages-forms.component.html',
  styleUrls: ['./messages-forms.component.css']
})
export class MessagesFormsComponent implements OnInit {
  //בניית ערוץ התקשורת בין message form to message
  @Output() addMessage:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
  @Output() addMessagePs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

  service:MessagesService;
  //Reactive Form
  //מתאים בדיוק לטופס עצמו
  msgform = new FormGroup({
      message:new FormControl(),
      user:new FormControl()
  });

  //שליחת הנתונים
  sendData() {
      //הפליטה של העדכון לאב
    this.addMessage.emit(this.msgform.value.message);
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
        response => {
          console.log(response.json());
          this.addMessagePs.emit();
        }
      );
  }

  constructor(service: MessagesService) {
    this.service = service;
   }

  ngOnInit() {
  }

}
