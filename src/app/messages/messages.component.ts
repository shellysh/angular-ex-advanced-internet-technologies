import { Component, OnInit } from '@angular/core';
import { MessagesService } from './messages.service';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  messages;
  messagesKeys=[];

  constructor(private service:MessagesService) {
    service.getMessages().subscribe(
      response=>{
        this.messages = response.json();
        this.messagesKeys = Object.keys(this.messages);
    }); //הרשמה ל observable
  }

  //הפונקציה שמוסיפה את המידע לטבלה באופן אופטימיסטיק
  optimisticAdd(message){
      //console.log("addMessage work "+message); בדיקה
      var newKey = parseInt(this.messagesKeys[this.messagesKeys.length - 1],0) + 1;
      var newMessageObject = {};
      newMessageObject['body'] = message; //גוף ההודעה עצמה
      this.messages[newKey] = newMessageObject;
      this.messagesKeys = Object.keys(this.messages);
    }
  
    //תזמון
  pessimisticAdd(){
      this.service.getMessages().subscribe(
        response=>{
          this.messages = response.json();
          this.messagesKeys = Object.keys(this.messages);
      });
  }

  //מחיקת רשומה
  deleteMessage(key){
      console.log.apply(key);
      let index = this.messagesKeys.indexOf(key);
      this.messagesKeys.splice(index,1); //אחד מבטא מחיקת רשומה אחת
  
      //delete from server
      this.service.deleteMessage(key).subscribe(
        response=>console.log(response)
      );
  }
   
  ngOnInit() {
  }

}
