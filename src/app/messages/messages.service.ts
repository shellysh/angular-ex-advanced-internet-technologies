import { Injectable } from '@angular/core';
import { Http , Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class MessagesService {
  http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי

  getMessages(){
    return this.http.get('http://localhost/ex/messages');
  }

  postMessage(data){
        //המרת גייסון למפתח וערך
        let options = {
          headers: new Headers({
            'content-type':'application/x-www-form-urlencoded'
          })
        }
    
        let params = new HttpParams().append('message',data.message);
        
        return this.http.post('http://localhost/ex/messages', params.toString(), options);
  }

  putMessage(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('message',data.message);
    return this.http.put('http://localhost/ex/messages/'+ key,params.toString(), options);
  }

  deleteMessage(key){
      return this.http.delete('http://localhost/ex/messages/'+ key);
  }

  getMessage(id){
       return this.http.get('http://localhost/ex/messages/'+ id);
  }

  constructor(http:Http) { 
    this.http = http;
  }

}
